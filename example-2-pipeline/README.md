# course-jenkins

**Pipeline**

คือ การสร้างคำสั่งด้วยการ เขียนเป็น script เอาไว้ เพื่อง่ายต่อการย้ายไปใช้งานต่อในอนาคต
ใช้ภาษา groovy script ในการเขียน

แบ่งออกเป็น 2 วิธี
1. เขียนฝั่งไว้ใน pipeline
 - ง่าย แก้ไขได้และทดสอบได้เลย
2. เขียนเก็บไว้ในไฟล์ แล้วเก็บไว้ใน source control ซึ่งเรียกไฟล์ดังกล่าวว่า Jenkinfiles
 - แก้จากไฟล์ใน source control
 - มี versioning แก้ไปโดยใครก็รู้ และมีประวัติการแก้ไขไฟล์
 - แยกเป็น function ได้ ง่ายต่อการ reuse


**Lab**

1. ทดลองสร้าง Pipeline เพื่อ download sourcecode
 > ```https://gitlab.com/isnoopy/course-jenkins/-/raw/master/example-2-pipeline/file.txt```
2. แสดงรายการไฟล์
3. แสดงเนื้อหาไฟล์

**More Lab**
- [https://www.jenkins.io/doc/book/pipeline/](https://www.jenkins.io/doc/book/pipeline/)
- [https://www.jenkins.io/doc/book/pipeline/syntax/](https://www.jenkins.io/doc/book/pipeline/syntax/)