# course-jenkins

**Freestyle Project**

คือ การสร้างคำสั่งด้วยการ config ผ่านหน้าเว็บ เหมาะกับงานที่ไม่ได้ซับซ้อน

**Lab**

1. ทดลองสร้าง Freestyle Project เพื่อ download sourcecode
 > ```https://gitlab.com/isnoopy/course-jenkins/-/raw/master/example-1-freestyle/file.txt```
2. แสดงรายการไฟล์
3. แสดงเนื้อหาไฟล์
4. เมื่อทำงานเสร็จให้ jenkin ส่งเสียงเตือน
